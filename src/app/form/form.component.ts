import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  @ViewChild('SecondName') secondNameRef: ElementRef;
  firstName;
  secondName;

  constructor() { }

  ngOnInit() {
  }

  moveSecondName() {
    const  htmlElement = this.secondNameRef.nativeElement as HTMLInputElement;
    this.secondName = htmlElement.value;
  }
}
